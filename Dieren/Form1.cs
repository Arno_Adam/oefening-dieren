﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace Dieren
{
    public partial class Form1 : Form
    {
        ArrayList dieren = new ArrayList();

        public Form1()
        {
            InitializeComponent();
        }


        private void OverzichtAfdrukken(ArrayList arrayList, TextBox textBox)
        {
            textBox.Text = "";
            for (int i = 0; i < arrayList.Count; i++)
            {
                textBox.Text += arrayList[i] + Environment.NewLine;
            }

        }


        private void button1_Click(object sender, EventArgs e)
        {
            //inlezen
            string path = @"C:\Users\arnoa\source\repos\Dieren\Les 16 - Dieren.txt"; //de @ zorgt ervoor dat de \ niet als speciaal teken wordt gezien

            StreamReader reader = new StreamReader(path);
            string line = "";
            while (!reader.EndOfStream) 
            {
                line = reader.ReadLine();
                if (!dieren.Contains(line))
                {
                    dieren.Add(line);
                }
            }
            reader.Close();

            OverzichtAfdrukken(dieren, txtOverzicht);
        }

        

        private void button2_Click(object sender, EventArgs e)
        {
            //toevoegen
            if (String.IsNullOrWhiteSpace(txtInput.Text)) 
            {
                MessageBox.Show("Eerst iets intypen om toe te voegen.");
            }
            else
            {
                dieren.Add(txtInput.Text);
                OverzichtAfdrukken(dieren, txtOverzicht);
                txtInput.Text = "";
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Wissen
            dieren.Clear();
            OverzichtAfdrukken(dieren, txtOverzicht);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Sorteren
            dieren.Sort();
            OverzichtAfdrukken(dieren, txtOverzicht);
        }
    }
}
